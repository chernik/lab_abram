#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <time.h>
#include <mem.h>
#include <malloc.h>

#define nmb long double

int N = 501;
#define M 50000.0
#define D 0.01
#define ITERS 200

void genData(nmb data[]){
    data[0] = 250;
    data[1] = 250;
    data[2] = 0;
    data[3] = 0;
    data[4] = 0;
    data[5] = 0;
    data[6] = 50;
    data[7] = 20;
    int i;
    for(i = 1; i < N; i++){
        data[i*8+0] = 350 + (i-1) * 20;
        data[i*8+1] = 250;
        data[i*8+2] = i;
        data[i*8+3] = 0;
        data[i*8+4] = (i-1) + 10;
        data[i*8+5] = 0;
        data[i*8+6] = 1;
        data[i*8+7] = 10;
    }
}

void saveData(nmb data[], char* nm){
    FILE* f = fopen(nm, "w");
    if(f == 0){
        perror("Cannot save file");
        return;
    }
    int i, j;
    double v;
    for(i = 0; i < N; i++){
        for(j = 0; j < 8; j++){
            v = data[i*8+j];
            fprintf(f, "%lf ", v);
        }
        fprintf(f, "\n");
    }
    fclose(f);
}

void mathSync(nmb data[]){
    int i, j, iter;
    nmb *mtx = (nmb*)malloc(N*N*3*sizeof(nmb));
    nmb vrx, vry, vrz, d, f;
    for(iter = 0; iter < ITERS; iter++){
        for(i = 0; i < N; i++){
            for(j = 0; j < N; j++){
                if(i == j){
                    mtx[i*N*3+j*3+0] = mtx[i*N*3+j*3+1] = mtx[i*N*3+j*3+2] = 0;
                    continue;
                }
                vrx = data[j*8+0] - data[i*8+0];
                vry = data[j*8+1] - data[i*8+1];
                vrz = data[j*8+2] - data[i*8+2];
                d = powl(vrx*vrx+vry*vry+vrz*vrz, 0.5);
                if(d == 0){
                    mtx[i*N*3+j*3+0] = mtx[i*N*3+j*3+1] = mtx[i*N*3+j*3+2] = 0;
                    continue;
                }
                f = data[j*8+6] * M / d / d;
                f = (d < data[i*8+7] + data[j*8+7]) ? -f : f;
                vrx = vrx * f / d;
                vry = vry * f / d;
                vrz = vrz * f / d;
                mtx[i*N*3+j*3+0] = vrx * D;
                mtx[i*N*3+j*3+1] = vry * D;
                mtx[i*N*3+j*3+2] = vrz * D;
            }
        }
        for(i = 0; i < N; i++){
            for(j = 0; j < N; j++){
                data[i*8+3] += mtx[i*N*3+j*3+0];
                data[i*8+4] += mtx[i*N*3+j*3+1];
                data[i*8+5] += mtx[i*N*3+j*3+2];
            }
            data[i*8+0] += data[i*8+3];
            data[i*8+1] += data[i*8+4];
            data[i*8+2] += data[i*8+5];
        }
    }
}

double mathAsync(nmb data[]){
    nmb *mtx = (nmb*)malloc(N*N*3*sizeof(nmb));
    double cs, cf;
    #pragma omp parallel default(none) shared(data, mtx, cs, cf, N)
    {
        nmb vrx, vry, vrz, d, f;
        int i, j, ij, iter;
        #pragma omp master
        {
            cs = omp_get_wtime();
        }
        #pragma omp barrier
        for(iter = 0; iter < ITERS; iter++){
            #pragma omp for
            for(ij = 0; ij < N*N; ij++){
                i = ij / N;
                j = ij % N;
                if(i == j){
                    mtx[ij*3+0] = mtx[ij*3+1] = mtx[ij*3+2] = 0;
                    continue;
                }
                vrx = data[j*8+0] - data[i*8+0];
                vry = data[j*8+1] - data[i*8+1];
                vrz = data[j*8+2] - data[i*8+2];
                d = powl(vrx*vrx+vry*vry+vrz*vrz, 0.5);
                if(d == 0){
                    mtx[ij*3+0] = mtx[ij*3+1] = mtx[ij*3+2] = 0;
                    continue;
                }
                f = data[j*8+6] * M / d / d;
                f = (d < data[i*8+7] + data[j*8+7]) ? -f : f;
                vrx = vrx * f / d;
                vry = vry * f / d;
                vrz = vrz * f / d;
                mtx[ij*3+0] = vrx * D;
                mtx[ij*3+1] = vry * D;
                mtx[ij*3+2] = vrz * D;
            }
            #pragma omp for
            for(i = 0; i < N; i++){
                for(j = 0; j < N; j++){
                    data[i*8+3] += mtx[i*N*3+j*3+0];
                    data[i*8+4] += mtx[i*N*3+j*3+1];
                    data[i*8+5] += mtx[i*N*3+j*3+2];
                }
                data[i*8+0] += data[i*8+3];
                data[i*8+1] += data[i*8+4];
                data[i*8+2] += data[i*8+5];
            }
        }
        #pragma omp barrier
        #pragma omp master
        {
            cf = omp_get_wtime();
        }
    }
    return cf - cs;
}

int main(int argc, char *argv[])
{
    int threads = 0;
    omp_set_dynamic(0);

    printf("Count of threads > ");
    scanf("%d", &threads);
    printf("Count of objects > ");
    scanf("%d", &N);

    nmb *data_sync = (nmb*)malloc(N*8*sizeof(nmb));
    nmb *data_async = (nmb*)malloc(N*8*sizeof(nmb));
    struct timeval sc, fc;

    genData(data_sync);
    memcpy(data_async, data_sync, N*8*sizeof(nmb));

    gettimeofday(&sc, NULL);
    mathSync(data_sync);
    gettimeofday(&fc, NULL);
    saveData(data_sync, "dataout_sync.txt");
    printf("\nSync prog\n");
    printf("\nTime [%lf sec]\n", (double)(fc.tv_sec * 1000000 + fc.tv_usec - sc.tv_sec * 1000000 - sc.tv_usec) / 1000000);

    omp_set_num_threads(threads);
    memcpy(data_sync, data_async, N*8*sizeof(nmb));
    mathAsync(data_sync);
    double timemesh = mathAsync(data_async);
    saveData(data_async, "dataout_async.txt");
    printf("\nAsync prog with threads [%d]\n", threads);
    printf("\nTime [%lf sec]\n", timemesh);

    return 0;
}
