var data1 = [
    [100, 250, 0, 1, 20, 100],
    [400, 250, 0, -1, 20, 100],
    [250, 250, 1, 0, 1, 15]
];

var data = (function(){
    var res = [];
    res.push([250, 250, 0, 0, 50, 20])
    for(var i = 0; i < 50; i++){
        res.push([350 + i * 20, 250, 0, i + 10, 1, 10]);
    }
    return res;
})();

var M = 50000;
var D = 0.01;
var S = 2;
var ITERS = 2000;

async function saveData(){
    var txt = data.map(e => e.join(' ')).join('\n');
    console.log(txt);
}

async function main(){
    var cnv = document.createElement('canvas').getContext('2d');
    document.body.appendChild(cnv.canvas);
    cnv.canvas.width = cnv.canvas.height = 1000;
    var dd = 50;
    await saveData();
    for(var iter = 0; iter < ITERS; iter++){
        // Draw
        cnv.canvas.width += 0; // comment for traectory
        cnv.beginPath();
        for(var i = 0; i < data.length; i++){
            cnv.moveTo(data[i][0] - S, data[i][1] - S);
            cnv.lineTo(data[i][0] + S, data[i][1] + S);
            cnv.moveTo(data[i][0] - S, data[i][1] + S);
            cnv.lineTo(data[i][0] + S, data[i][1] - S);
            cnv.strokeText('' + i, data[i][0] - S, data[i][1] - S);
        }
        cnv.closePath();
        cnv.stroke();
        // Match next
        var sms = [];
        for(var i = 0; i < data.length; i++){
            var sm = [data[i][2], data[i][3]];
            for(var j = 0; j < data.length; j++){
                if(i == j){
                    continue;
                }
                var vr = [data[j][0] - data[i][0], data[j][1] - data[i][1]];
                var d = Math.sqrt(vr[0]*vr[0] + vr[1]*vr[1]);
                if(d == 0){
                    continue;
                }
                var f = data[j][4] * M / d / d;
                var f = (d < data[i][5] + data[j][5]) ? -f : f;
                if(f < 0){
                    //dd = 1000;
                }
                vr = [vr[0] * f / d, vr[1] * f / d];
                sm[0] += vr[0] * D;
                sm[1] += vr[1] * D;
            }
            sms.push(sm);
        }
        for(var i = 0; i < data.length; i++){
            data[i][0] += sms[i][0];
            data[i][1] += sms[i][1];
            data[i][2] = sms[i][0];
            data[i][3] = sms[i][1];
        }
        // Wait
        await new Promise(cb => setTimeout(cb, dd));
    }
    await saveData();
}

window.onload = main;
